import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';
import { Navigate, useNavigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [password2, setPassword2] = useState("");
	const [yourName, setYourName] = useState("");

	const [isActive, setIsActive] = useState(false);

	function registerUser (e) {

		e.preventDefault();

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if (data === true) {

				Swal.fire({
					title: "Duplicate Email Found",
					icon: "error",
					text: "Kindly provide another email to complete registration."
				})
			} else {

				fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						email: email,
						password: password,
						yourName: yourName
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)

					if(data === true) {

						// Clear input fields
						setEmail("");
						setPassword("");
						setPassword2("");
						setYourName("")

						Swal.fire({
							title: "Registration Successful",
							icon: "success",
							text: "Welcome to Zuitt!"
						})

						navigate("/login");

					} else {

						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please, try again."
						})
					}

				})
			}
		})

	}


	useEffect(() => {
		if((email !== "" && password !== "" && password2 !== "" && yourName !== "") && (password === password2)) {

			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password, password2, yourName])


	return(

		(user.id !== null) ?
			<Navigate to="/home"/>

			:

			<Form onSubmit={(e) => registerUser(e)}  className="col-md-4 mx-auto">

				  <h1 className="text-center my-3">create an account</h1>

				  <Form.Group className="mb-3" controlId="userEmail">
			        <Form.Label>Email address</Form.Label>
			        <Form.Control 
			        	type="email"
			        	value={email}
			        	onChange={(e) => {setEmail(e.target.value)}}
			        	placeholder="Enter email" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password">
			        <Form.Label>Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	value={password}
			        	onChange={(e) => {setPassword(e.target.value)}}
			        	placeholder="Enter Your Password" />
			      </Form.Group>

			      <Form.Group className="mb-3" controlId="password2">
			        <Form.Label>Verify Password</Form.Label>
			        <Form.Control 
			        	type="password" 
			        	value={password2}
			        	onChange={(e) => {setPassword2(e.target.value)}}
			        	placeholder="Verify Your Password" />
			      </Form.Group>

				  <Form.Group className="mb-3" controlId="yourName">
				    <Form.Label>Enter your name</Form.Label>
				    <Form.Control 
				    	type="text"
				    	value={yourName}
				    	onChange={(e) => {setYourName(e.target.value)}}
				    	placeholder="Enter your Name" 
				    	required
				    	/>
				    	<Form.Text className="text-muted">
			          What should we call you?
			        </Form.Text>
				  </Form.Group>

			      { isActive ?
			      			<Button variant="primary" type="submit" id="submitBtn">
			      		 	 Submit
			      			</Button>
			      			:
			      			<Button variant="primary" type="submit" id="submitBtn" disabled>
			      			  Submit
			      			</Button>
			      }
			     
			    </Form>	
		

	)
}