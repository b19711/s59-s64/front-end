//import { useState, useEffect } from 'react';
import { Row, Col, Button, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
//import Resources from './Resources'

export default function ProductCard({productProp}){

    // checks to see if the data wass successfully passed
    //console.log(productsData);

    // Every component receives in a form of an object
    //console.log(console.log[0]);

    //console.log({productProp});

    const { name, description, price, _id } = productProp;
  


  return(

    <Row>
      <Col lg={{span:6, offset:3}}>
        <Card className="ProductCard my-3">
            <Card.Body>
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>{price}</Card.Text>
              <Button className="bg-primary" as={Link} to={`/products/${_id}`} >View</Button>
              </Card.Body>
          </Card>
      </Col>
    </Row>

  )
}