// New practice in importing components;
import { useContext } from 'react';
import { Form, Button, Row, Col, Card } from 'react-bootstrap';


export default function Footer() {

	return (
		<Row id="footer">
			<Col lg="4" sm="12" className='mb-4 mb-md-0 p-5'>
				<span><img
		              src={require('./logo.png')}
		              //width="30"
		              height="30"
		              alt="D&M AutoParts"
		            /></span>
				<p>D&M Auto Parts was established in 2022 to cater a large automotive market place in the Philippines. We are the recognized leader in our field, dedicated to quality in every area of our business and respected for our outstanding business ethics. </p>
			</Col>

			<Col lg="4" sm="12" className='mb-4 mb-md-0 p-5'>
				<h5>visit us</h5>
				<p>123 Four-five St., Sixseven City, Eight, PH</p>

				<h5>follow us</h5>
				<p>123 Four-five St., Sixseven City, Eight, PH</p>

			</Col>

			<Col lg="4" sm="12" className='mb-4 mb-md-0 p-5'>
				<Form>
					<Form.Group className="mb-3" controlId="userEmail">
						<Form.Control type="email" placeholder="Enter email" />
					</Form.Group>

					<Form.Group className="mb-3" controlId="userEmail">
						<Form.Control as="textarea" rows={4} placeholder="Enter email" />
					</Form.Group>

					<Button variant="primary" type="submit" id="submitBtn">Send us a message</Button>
				</Form>
			</Col>
		</Row>
	)
}
