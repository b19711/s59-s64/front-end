import Carousel from 'react-bootstrap/Carousel';

export default function DarkVariantExample() {
  return (
    <Carousel variant="light" className="banner">
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../img/banner1.jpg')}
          alt="First slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../img/banner2.jpg')}
          alt="Second slide"
        />
      </Carousel.Item>
      <Carousel.Item>
        <img
          className="d-block w-100"
          src={require('../img/banner3.jpg')}
          alt="Third slide"
        />
      </Carousel.Item>
    </Carousel>
  );
}